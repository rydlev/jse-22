package ru.t1.rydlev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@NotNull String userId, @Nullable String projectId);

    void removeProjects();

    @NotNull
    Task unbindTaskFromProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

}
