package ru.t1.rydlev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M add(M model);

    boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    M removeByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<M> collection);

}
