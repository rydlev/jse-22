package ru.t1.rydlev.tm.exception.system;

public class AlgorithmNotAvailableException extends AbstractSystemException {

    public AlgorithmNotAvailableException() {
        super("Error! Algorithm is not available in the environment...");
    }

    public AlgorithmNotAvailableException(final String algorithm) {
        super("Error! Algorithm \"" + algorithm + "\" is not available in the environment...");
    }

}
